#use wml::debian::template title="Debian 發行版本"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f"

<p>Debian 一直維護著至少\
三個發行版本：<q>穩定版（stable）</q>，<q>測試版（testing）</q>和\
<q>不穩定版（unstable）</q>。</p>

<dl>
<dt><a href="stable/">穩定版（stable）</a></dt>
<dd>
<p>
  <q>穩定版</q>包含了 Debian 官方最近一次\
發行的軟件包。
</p>
<p>
  作為 Debian 的正式發行版本，它是我們優先\
推薦給用戶您選用的版本。
</p>
<p>
  當前 Debian 的<q>穩定版</q>版本號是
  <:=substr '<current_initial_release>', 0, 2:>，開發代號為 <em><current_release_name></em>。\
<ifeq "<current_initial_release>" "<current_release>"
  "于 <current_release_date> 發布。"
/>\
<ifneq "<current_initial_release>" "<current_release>"
  "最初版本为 <current_initial_release>，\
于 <current_initial_release_date> 發布，其更新
<current_release> 已于 <current_release_date> 发布。"
/>
</p>
</dd>

<dt><a href="testing/">測試版（testing）</a></dt>
<dd>
<p>
  <q>測試版</q>包含了那些暫時未被\
收錄進入<q>穩定版</q>的軟件包，但它們已經進入\
了候選隊列。使用這個版本的最大益處在于它擁有更多\
版本較新的軟件。
</p>
<p>
  想要了解 <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">什麼是<q>測試版</q></a>\
以及 <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">如何成為\
<q>穩定版</q></a>的更多信息，\
請看 <a href="$(DOC)/manuals/debian-faq/">Debian FAQ</a>。
</p>
<p>
  當前的<q>測試版</q>版本代號是 <em><current_testing_name></em>。
</p>
</dd>

<dt><a href="unstable/">不穩定版（unstable）</a></dt>
<dd>
<p>
  <q>不穩定版</q>存放了 Debian 現行的\
開發工作。通常，只有開發者和那些\
喜歡過驚險刺激生活的人選用該版本。\
推荐使用<q>不稳定版</q>的用户订阅 debian-devel-announce \
邮件列表，以接收关于重大变更的通知，比如\
有可能导致问题的升级。
</p>
<p>
  <q>不穩定版</q>的版本代號永遠都被稱為 <em>sid</em>。
</p>
</dd>
</dl>

<h2>发行生命周期</h2>
<p>
  Debian 通常会按照一定的规律每隔一段时间发布一个新稳定版。
  对每个稳定发行版本，用户可以得到三年的完整支持以及额外两年的\
长期支持。
</p>

<p>
  请查看 <a href="https://wiki.debian.org/DebianReleases">Debian Releases</a>
  维基页面和 <a href="https://wiki.debian.org/LTS">Debian LTS</a>
  维基页面以了解详细信息。
</p>

<h2>發行版目錄</h2>

<ul>

  <li><a href="<current_testing_name>/">下一代 Debian 正式發行版的代號為
    <q><current_testing_name></q></a>
      &mdash; <q>测试（testing）</q>版 &mdash; 发布日期尚未确定
  </li>

  <li><a href="bullseye/">Debian 11 (<q>bullseye</q>)</a>
      &mdash; 当前的<q>稳定（stable）</q>版
  </li>

  <li><a href="buster/">Debian 10（<q>buster</q>）</a>
      &mdash; 當前的<q>旧的穩定（oldstable）</q>版
  </li>

  <li><a href="stretch/">Debian 9（<q>stretch</q>）</a>
      &mdash; <q>更旧的穩定（oldoldstable）</q>版，现有<a href="https://wiki.debian.org/LTS">长期支持</a>
  </li>

  <li><a href="jessie/">Debian 8（<q>jessie</q>）</a>
      &mdash; 已存档版本，现有<a href="https://wiki.debian.org/LTS/Extended">扩展长期支持</a>
  </li>

  <li><a href="wheezy/">Debian 7（<q>wheezy</q>）</a>
      &mdash; 被淘汰的穩定版
  </li>

  <li><a href="squeeze/">Debian 6.0（<q>squeeze</q>）</a>
      &mdash; 被淘汰的穩定版
  </li>

  <li><a href="lenny/">Debian GNU/Linux 5.0（<q>lenny</q>）</a>
      &mdash; 被淘汰的穩定版
  </li>

  <li><a href="etch/">Debian GNU/Linux 4.0（<q>etch</q>）</a>
      &mdash; 被淘汰的穩定版
  </li>
 <li><a href="sarge/">Debian GNU/Linux 3.1（<q>sarge</q>）</a>
     &mdash; 被淘汰的穩定版
 </li>
  <li><a href="woody/">Debian GNU/Linux 3.0（<q>woody</q>）</a>
      &mdash; 被淘汰的穩定版
  </li>
  <li><a href="potato/">Debian GNU/Linux 2.2（<q>potato</q>）</a>
      &mdash; 被淘汰的穩定版
  </li>
  <li><a href="slink/">Debian GNU/Linux 2.1（<q>slink</q>）</a> 
      &mdash; 被淘汰的穩定版
  </li>
  <li><a href="hamm/">Debian GNU/Linux 2.0（<q>hamm</q>）</a>
      &mdash; 被淘汰的穩定版
  </li>
</ul>

<p>被淘汰的 Debian 正式發行版的網頁信息已被原封不動地保留，但是\
這些發行版本身只能在單獨的\
<a href="$(HOME)/distrib/archive">[CN:存档:][HKTW:檔案庫:]</a>中找到。</p>

<p>請參考 <a href="$(HOME)/doc/manuals/debian-faq/">Debian FAQ</a> 中關于\
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">所有\
這些版本代號的由來</a>的相關說明信息。</p>

<h2>發行版中數據的完整性</h2>

<p>數據的完整性由一個經[CN:數字簽名:][HK:数码签章:][TW:数位签章:]的 \
<code>Release</code> [CN:文件:][HKTW:档案:]\
来保证。為了保證發行版中的每個[CN:文件:][HKTW:档案:]確實屬于該發行版，\
每個 <code>Packages</code> [CN:文件:][HKTW:档案:]的校驗碼\
都被復制到 <code>Release</code> [CN:文件:][HKTW:档案:]中。</p>

<p>該[CN:文件:][HKTW:档案:]的[CN:數字簽名:][HK:数码签章:][TW:数位签章:]\
被保存在[CN:文件:][HKTW:档案:] \
<code>Release.gpg</code> 中，用的是[CN:當前:][HKTW:目前:]版本的存檔\
[CN:簽名:][HKTW:签章:][CN:密鑰:][HKTW:金钥:]。\
对于<q>穩定版</q>（stable）與<q>旧的穩定版</q>（oldstable）\
而言，还会使用一个[CN:额外:][HKTW:附加:]的[CN:簽名:][HKTW:签章:]，它是\
由<a href="$(HOME)/intro/organization#release-team">\
穩定版[CN:发布:][HKTW:释出:]團隊</a>的成員\
用一把專門用于[CN:发布:][HKTW:释出:]版本\
的離線[CN:密鑰:][HKTW:金钥:]製作而成。</p>
