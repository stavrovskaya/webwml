#use wml::debian::template title="使用 git 操作网站源代码"
#use wml::debian::translation-check translation="74a4415fa07f5810685f1d0568bf4b695cc931b3"

<h2>介绍</h2>

<p>Git 是一个<a href="https://en.wikipedia.org/wiki/Version_control">版本控制系统</a>\
，用于帮助在同一份资料上的多人协作。每个用户都可以拥有主存储库的本地副本。本地副本可以\
位于同一台计算机上，也可以位于世界各地。然后，用户可以根据需要修改本地副本，并在完成\
资料的修改后，提交更改并将其推回主存储库。</p>


<p>如果远程仓库有比您在同一个分支上的本地副本新的提交（修改），则 Git 不允许您直接推送提交。\
如果发生冲突，请先获取并更新您的本地副本，然后在最新提交的基础上根据需要<code>rebase</code>\
您的新修改。</p>

<h3><a name="write-access">Git 存储库写入权限</a></h3>

<p>整个 Debian 网站源代码都使用 Git 管理。它位于 <url https://salsa.debian.org/webmaster-team/webwml/>。\
默认情况下，不允许访客推送自己的提交到源代码存储库中。您需要一些许可才能获得对存储库的写入权。</p>

<h4><a name="write-access-unlimited">不受限制的写入权限</a></h4>

<p>如果您需要对存储库的不受限制的写入权限（例如，您打算成为频繁提交的贡献者），请考虑通过 \
<url https://salsa.debian.org/webmaster-team/webwml/>网页登录到 Debian Salsa 平台后请求写入\
权限。</p>

<p>如果您刚开始参与 Debian 网站开发，并且没有经验，请在请求不受限制的写入访问权限之前发送\
一封带有自我介绍的电子邮件到<a href="mailto:debian-www@lists.debian.org">
debian-www@lists.debian.org</a>。请考虑在自我介绍中提供一些有用的信息，例如您打算翻译哪种语言\
或网站的哪个部分，以及谁会为您担保。</p>

<h4><a name="write-access-via-merge-request">通过合并请求（Merge Requests）写入存储库</a></h4>

<p>如果您不打算获得对存储库的不受限制的写入权限或无法做到这一点，您可以随时提交合并请求，\
并让其他开发人员检查并接受您的成果。请使用由 Salsa GitLab 平台通过其网页界面提供的标准程序提交\
合并请求（阅读 \
<a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a> \
以及 \
<a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a> \
了解详细步骤）。</p>

<p>并不是所有的网站开发人员都随时查看合并请求，因此他们不一定总是及时处理。如果您不确定所作的\
贡献是否会被接受，请发送电子邮件至<a href="https://lists.debian.org/debian-www/"> debian-www </a>\
邮件列表请求核查。</p>

<h2><a name="work-on-repository">使用存储库开展工作</a></h2>

<h3><a name="get-local-repo-copy">获取一份本地副本</a></h3>

<p>首先，您需要安装 git 才能使用存储库。接下来，在您的计算机上设置您的用户名和电子邮件信息（请参阅\
通常的 git 文档以了解如何执行此操作）。然后，您可以采用以下两种方式之一克隆存储库（换句话说，对其\
创建本地副本）。</p>

<p>建议使用 webwml 的方法是先在 salsa.debian.org 上注册一个帐户，并通过将 SSH 公钥上传到您的 salsa 帐户\
来启用 git SSH 访问。有关如何执行此操作的更多详细信息，请参见<a 
href="https://salsa.debian.org/help/ssh/README.md"> salsa 帮助页面</a>。然后，您可以使用以下命令\
克隆 webwml 存储库：</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>如果您还没有 Salsa 帐号，您可以使用 HTTPS 协议克隆并获取仓库副本：</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>这将为您提供相同的存储库本地副本，但是您将无法直接以这种方式将修改直接推送回远程仓库。</p>

<p>克隆整个 webwml 存储库将需要下载大约 500MB 的数据，因此对于网络连接缓慢或不稳定的用户而言可能\
会很麻烦。您可以先尝试以最小深度进行浅克隆，以进行较小数据的初始下载：</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>获得可用（浅）存储库后，您可以加深本地浅副本，并最终将其转换为完整的本地存储库：</p>

<pre>
  git fetch --deepen=1000 # 为仓库加深另 1000 次提交
  git fetch --unshallow   # 获取所有未获取的提交，将仓库转换成完整的
</pre>

<h4><a name="partial-content-checkout">检出部分内容</a></h4>

<p>您可以仅检出部分页面，如下所示：</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
   在 webwml 中: 创建带有像这样内容的文件 .git/info/sparse-checkout ：
   (如果您只想要基本文件、英语、加泰罗尼亚语和西班牙语翻译):
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
   然后:
   $ git checkout --
</pre>

<h3><a name="submit-changes">提交本地修改</a></h3>

<h4><a name="keep-local-repo-up-to-date">令您的本地仓库保持最新</a></h4>

<p>每隔几天（也一定要在开始一些编辑工作之前！），您应该</p>

<pre>
   git pull
</pre>

<p>以从存储库中获取已被更改的文件。</p>

<p>强烈建议在执行“git pull”和进行编辑工作之前保持本地 git 工作目录的干净。如果您有\
未提交的更改或当前分支的远程存储库中不存在的本地提交，则执行“git pull”将会自动\
创建合并提交，甚至会由于冲突而导致拉取失败。请考虑将未完成的工作保存在另一个分支\
中，或使用“git stash”之类的命令。</p>

<p>注意：git 是一个分布式（而非集中式）版本控制系统。这意味着当您提交更改时，它们将\
仅存储在本地存储库中。要与他人共享它们，您还需要将所做的更改推送到 salsa 上的中央存储库。</p>

<h4><a name="example-edit-english-file">编辑英语文件的示例</a></h4>

<p>此处提供了有关于如何在网站源代码存储库中编辑英语文件的示例。在使用“git clone”获取\
存储库的本地副本之后，也在开始编辑工作之前，运行以下命令：</p>

<pre>
   $ git pull
</pre>

<p>现在对文件进行修改。当您完成后，使用以下命令将您的更改提交到本地存储库：</p>

<pre>
   $ git add path/to/file(s)
   $ git commit -m "Your commit message"
</pre>

<p>如果您有对远程 webwml 存储库的不受限制的写入权限，那么现在可以将您所做的更改直接\
推送到 Salsa 存储库中：</p>

<pre>
   $ git push
</pre>

<p>如果您无权直接写入 webwml 存储库，请考虑使用 Salsa GitLab 平台提供的“合并请求”功能\
提交您的更改，或寻求其他开发人员的帮助。</p>

<p>这是有关于如何使用 git 操作 Debian 网站源代码的非常基本的概述。有关 git 的更多信息，\
请阅读 git 的文档。</p>

<h4><a name="closing-debian-bug-in-git-commits">在 git 提交中关闭 Debian 的错误</a></h4>

<p>如果您的提交日志条目中包含<code>Closes: #</code><var>nnnnnn</var>，那么在您推送\
您的更改后，错误编号<code>#</code><var>nnnnnn</var>将会被自动关闭。精确的形式与
<a href="$(DOC)/debian-policy/ch-source.html#id24">Debian 政策</a>相同。</p>

<h4><a name="links-using-http-https">使用 HTTP/HTTPS 链接</a></h4>

<p>许多 Debian 网站都支持 SSL/TLS，因此请尽可能使用 HTTPS 链接。<strong>但是</strong>，\
某些 Debian/DebConf/SPI 等网站要么不具有 HTTPS 支持，要么仅使用 SPI 数字证书认证机构\
（并不是一个被所有浏览器信任的 SSL 证书认证机构）。为了避免给非 Debian 用户带来错误消息\
的困扰，请不要使用 HTTPS 链接到此类站点。</p>

<p>git 存储库会拒绝那些包含使用纯 HTTP 链接到支持 HTTPS 的 Debian 网站的内容或包含使用 HTTPS\
链接到已知不支持 HTTPS 或仅使用 SPI 签名的证书的 Debian/DebConf/SPI 网站的内容的提交。</p>

<h3><a name="translation-work">翻译工作</a></h3>

<p>翻译应始终与相对应的英文文件一样保持最新。翻译文件中的“translation-check”头用于\
跟踪当前翻译所基于的英语文件版本。如果您更改了已翻译的文件，则需要更新 \
translation-check 头以匹配英语文件中相应更改的 git 提交哈希值。您可以使用以下方式找到\
该哈希值</p>

<pre>
$ git log path/to/english/file
</pre>

<p>如果您要对一个文件新建翻译，请使用<q>copypage.pl</q>脚本，它将为您的语言创建一个\
模板，该模板包含了正确的翻译头。</p>

<h4><a name="translation-smart-change">使用 smart_change.pl 进行翻译修改</a></h4>

<p><code>smart_change.pl </code>是一个脚本，旨在令一起更新原始文件及其翻译更为容易。\
有两种方法供您使用，选择哪种取决于您进行的更改。</p>

<p>当您在手动更改文件时使用<code> smart_change </code>更新 translation-check 头：</p>

<ol>
  <li>对原始文件进行更改，然后提交</li>
  <li>更新翻译</li>
  <li>运行 smart_change.pl——它会获取更改并更新已翻译文件的头</li>
  <li>查看更改（例如，使用“ git diff”）</li>
  <li>提交翻译更改</li>
</ol>

<p>或者，如果您要使用带有正则表达式的 smart_change 来一次性对文件进行多个更改：</p>

<ol>
  <li>运行<code> smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>查看更改（例如，使用<code>git diff</code>）
  <li>提交原始文件</li>
  <li>运行<code> smart_change.pl origfile1 origfile2</code>
    (即，这次<strong>没有正则表达式</strong>)；它现在只会更新已翻译文件的头</li>
  <li>最后，提交翻译更改</li>
</ol>

<p>这比前一个方法涉及更多的提交（需要两次提交），但由于 git 提交的哈希值的工作方式，\
这无法避免。</p>

<h2><a name="notifications">获取通知</a></h2>

<h3><a name="commit-notifications">接收提交通知</a></h3>

<p>我们已经在 Salsa 中配置好了 webwml 项目，以便在 IRC 频道 #debian-www 中显示提交。</p>

<p>如果您想要在 webwml 存储库中有提交时通过电子邮件接收通知，请按照以下步骤（仅需一次） \
通过 tracker.debian.org 订阅<q>www.debian.org</q>伪软件包并在其中激活<q>vcs</q>\
关键字：</p>

<ul>
  <li>打开网页浏览器并访问 <url https://tracker.debian.org/pkg/www.debian.org></li>
  <li>订阅<q>www.debian.org</q>伪软件包。（如果您尚未在其它情况下使用过  tracker.debian.org\
      ，则可以使用 SSO 通过验证或设置电子邮件和密码以进行注册）。</li>
  <li>跳转到<url https://tracker.debian.org/accounts/subscriptions/>，然后<q>修改关键字</q>\
      , 选中 <q>vcs</q>（如果其未被选中）并保存。</li>
  <li>从现在开始，每当有人提交更改到 webwml 存储库时，您将收到电子邮件。我们将很快添加其他\
      网站管理员团队存储库。</li>
</ul>

<h3><a name="merge-request-notifications">接收合并请求通知</a></h3>

<p>
如果您想要在 Salsa GitLab 平台上的 webwml 仓库收到新的合并请求（Merge Request）\
的时候得到电子邮件提醒，您可以在网页界面上配置您的通知设置。步骤如下：
</p>

<ul>
  <li>登陆您的 Salsa 帐号并前往项目页面；</li>
  <li>点击项目主页顶部的通知图标（类似一个钟）；</li>
  <li>选择您所需的通知级别。</li>
</ul>
