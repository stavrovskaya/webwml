#use wml::debian::template title="Debian 2.1 (slink) -- Informações " BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/slink/formats_and_architectures.wmh"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd"

<:

$about_lang_setting = "If you have set your browser's localization
properly, you can use the above link to get the right HTML version
automatically -- see <A href=\"$base_url/intro/cn\">content negotiation</A>.
Otherwise, pick the exact architecture, language, and format you want
from the table below.\n";

 :>


<ul>
	<li><a href="#release-notes">Notas de lançamento</a>
	<li><a href="#new-inst">Novas instalações</a>
	<li><a href="#errata">Errata</a>
	<li><a href="#unofficial-updates">Atualizações não oficiais</a>
	<li><a href="#acquiring">Obtendo o Debian 2.1</a>
      </ul>

<p>

<strong>O Debian 2.1 foi substituído.</strong>

<p>

Desde que as <a href="../">versões mais recentes</a> foram feitas, a versão 2.1
foi substituída. Essas páginas estão sendo mantidas para fins históricos. Você
deve estar ciente de que o Debian 2.1 não é mais mantido.

<p>

As seguintes arquiteturas são suportadas no Debian 2.1:

<ul>
<: foreach $arch (@arches) {
      print "<li> " . $arches{$arch} . "\n";
   } :>
</ul>


<h2><a name="release-notes"></a>Notas de lançamento</h2>

<p>

Para descobrir o que há de novo no Debian 2.1, consulte as notas de lançamento
para a sua arquitetura. As notas de lançamento também contêm instruções para
usuários(as) que estão atualizando de versões anteriores.

<ul>
<: &permute_as_list('release-notes/', 'Release Notes'); :>
</ul>

<p>
<: print $about_lang_setting; :>
</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitetura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<: &permute_as_matrix('release-notes', 'english', 'croatian', 'czech',
		      'japanese', 'portuguese', 'russian');
:>
</table>
</div>

<p>

Para a arquitetura i386, está disponível um
<a href="i386/reports">relatório detalhado</a> que descreve os pacotes que
foram alterados desde os dois últimos lançamentos.

<h2><a name="new-inst"></a>Novas instalações</h2>

<p>

As instruções de instalação, junto com os arquivos para download, são
divididas em arquitetura:
<ul>
<:= &permute_as_list('install', 'Install Manual'); :>
</ul>

<p>
<: print $about_lang_setting; :>
</p>


<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitetura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<:= &permute_as_matrix('install', 'english', 'croatian', 'czech',
		      'finnish', 'french',
		      'japanese', 'portuguese', 'russian', 'spanish');
:>
</table>
</div>

Observe que a documentação alemã também está disponível, mas apenas para a
arquitetura i386; você pode ver isso em
<a href="i386/install.de.txt">ASCII</a>,
<a href="i386/install.de.pdf">PDF</a> ou
<a href="i386/install.de.html">HTML</a>.

<p>

Muitos esforços foram colocados no manual de instalação do Debian para suportar
versões não i386. No entanto, algumas arquiteturas (notadamente
<:=$arches{'alpha'}:>) ainda precisam de trabalho - consulte o próprio
documento para obter informações sobre como você pode contribuir.

<p>

Essas páginas web serão atualizadas com novas versões do manual de instalação
para o slink assim que estiverem disponíveis. Se você deseja contribuir com
alterações, consulte a seção e o manual; você também pode obter o
<a href="source/">fonte SGML</a> -- os patches são a melhor forma para
comentários. Os colaboradores(as) ou leitores(as) que se perguntam o que há
exatamente de novo devem ver o <a href="source/ChangeLog">ChangeLog</a>.


<h2><a name="errata"></a>Errata</h2>

<p>

Às vezes, no caso de problemas críticos ou atualizações de segurança, a versão
já lançada (neste caso, o slink) é atualizada. Geralmente eles são indicados
como lançamentos pontuais. A atual versão pontual é o Debian 2.1r5. Você pode
encontrar o
<a href="http://archive.debian.org/debian/dists/slink/ChangeLog">ChangeLog</a>
em qualquer espelho do repositório Debian.

<p>

As correções na versão estável (stable) lançada geralmente passam por um longo
período de testes antes de serem aceitas no repositório. No entanto, essas
correções estão disponíveis no repositório
<a href="http://archive.debian.org/debian/dists/slink-proposed-updates/">dists/slink-proposed-updates</a>
de qualquer espelho do repositório Debian. Se você usa o <tt>apt</tt> para
atualizar seus pacotes, você poderá instalar as atualizações propostas
adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:
<pre>
  deb http://archive.debian.org dists/slink-proposed-updates/
</pre>
Depois, execute <kbd>apt-get update; apt-get upgrade</kbd>.

<p>

O slink é certificado para uso com a série 2.0.x de kernels do Linux. Se você
deseja executar o kernel do Linux 2.2.x com o slink, consulte a
<a href="running-kernel-2.2">lista de problemas conhecidos</a>.


<h2><a name="unofficial-updates"></a>Atualizações não oficiais</h2>

<p>

As versões Debian lançadas são atualizadas apenas em casos de erros críticos ou
problemas de segurança. No entanto, para conveniência do(a) usuário(a), existe
uma certa atualização de software que é disponibilizada não oficialmente por
outros(as) usuários(as) e desenvolvedores(as). O conteúdo desta seção não é
oficialmente suportado pelo Debian.

<h3>Disquetes de inicialização internacionalizados</h3>

<p>

Disquetes inicializáveis em português não oficiais estão disponíveis em
ftp://ftp2.escelsanet.com.br/debian/.


<h3>Disquete de recuperação</h3>

<p>

Os(As) usuários(as) das placas SCSI Adaptec 2940 e de outras controladoras SCSI
com o chipset aic7xxx provavelmente terão problemas com os disquetes de
inicialização padrão. Um(a) gentil  usuário(a) fez algumas experiências que
muitos(as) usuários(as) usarão para resolver seus problemas. Existem dois
disquetes alternativos de recuperação para a arquitetura i386 em
ftp://kalle.csb.ki.se/pub/.
Também existem kernels substitutos nesse local, que você pode usar para
substituir simplesmente os kernels existentes nos disquetes de inicialização.
Você precisará de drivers de disquete no local padrão.

<p>

Um conjunto concorrente e mais recente de disquetes de recuperação e drivers
para usuários(as) do Adaptec pode ser encontrado em
https://www.debian.org/~adric/aic7xxx/.

<h3>Gnome</h3>

<p>

Se você deseja rodar o Gnome mais recente do estável (stable), consulte
a atualização do GNOME para Debian 2.1
(http://www.gnome.org/start/debian-readme.html).

<h3>APT</h3>

<p>

Uma versão atualizada do <code>apt</code> está disponível no Debian, a partir
do 2.1r3. O benefício desta versão atualizada é principalmente poder lidar com
a instalação a partir de vários CD-ROMs. Isso torna desnecessária a opção de
aquisição do <code>dpkg-multicd</code> no <code>dselect</code>. No entanto,
seu CD-ROM 2.1 pode conter um <code>apt</code> mais antigo, então você pode
querer atualizar usando o que está agora no slink.

<h2><a name="acquiring"></a>Obtendo o Debian 2.1</h2>

<p>

O Debian está disponível eletronicamente ou a partir de fornecedores(as) de CD.

<h3>Comprando o Debian em CD</h3>

<p>

Mantemos uma <a href="../../CD/vendors/">lista de vendedores(as) de CD</a>
que vendem CDs do Debian 2.1.


<h3>Baixando o Debian pela Internet</h3>

<p>

Mantemos uma <a href="../../distrib/ftplist">lista de sites</a> que
espelham a distribuição.


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
