#use wml::debian::translation-check translation="eebf73f6f1306851590138e0b9fc3917cf4580a6"
<define-tag pagetitle>Termina a DebConf21 on-line</define-tag>

<define-tag release_date>2021-08-28</define-tag>
#use wml::debian::news

<p>
No sábado, 28 de agosto de 2021, a conferência anual dos(as)
desenvolvedores(as) e contribuidores(as) Debian chegou ao seu fim.
</p>

<p>
A DebConf21 realizou-se on-line pela segunda vez devido à pandemia
do coronavírus (COVID-19). 
</p>

<p>
Todas as sessões foram transmitidas, e várias foram as formas de participação:
via mensagens IRC, documentos de texto colaborativos e on-line,
e salas de reunião para conferência por vídeo.
</p>

<p>
Com 740 participantes registrados(as) de mais de 15 países e um
total de 70 palestras, sessões de debates,
"desconferências" (Birds of a Feather - BoF) e outras atividades,
a <a href="https://debconf21.debconf.org">DebConf21</a> foi um grande sucesso.
</p>

<p>
A estrutura, que havia sido montada para os eventos on-line passados, envolveram
Jitsi, OBS, Voctomix, SReview, nginx, Etherpad, e uma interface web para o
Voctomix que foi melhorada e usada com sucesso na DebConf21.
Todos os componentes da infraestrutura de vídeo são softwares livres e foram
configurados através do repositório público do
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a>,
da equipe de Vídeo.
</p>

<p>
A <a href="https://debconf21.debconf.org/schedule/">grade de programação</a> da DebConf21
incluiu uma grande variedade de eventos, agrupados em diversas trilhas:
</p>
<ul>
<li>Introdução ao Software Livre e ao Debian,</li>
<li>Empacotamento, políticas e infraestrutura Debian,</li>
<li>Administração de sistemas, automação e orquestração,</li>
<li>Nuvem e contêineres,</li>
<li>Segurança,</li>
<li>Comunidade, diversidade, divulgação local e contextos sociais,</li>
<li>Internacionalização, localização e acessibilidade,</li>
<li>Embarcados &amp; kernel,</li>
<li>Debian blends e distribuições derivadas do Debian,</li>
<li>Debian nas artes &amp; nas ciências,</li>
<li>entre outras.</li>
</ul>
<p>
As palestras foram transmitidas usando duas salas, e muitas dessas atividades
foram realizadas em diferentes idiomas: telugu, português, malaiala, canarês,
hindi, marati e inglês, o que permitiu que uma audiência diversa desfrutasse e
participasse.
</p>

<p>
Entre as atividades, a transmissão de vídeo exibiu os(as) patrocinadores(as) usuais
em loop, mas também exibiu clipes adicionais, incluindo fotos de DebConfs
passadas, fatos divertidos sobre o Debian e vídeos curtos enviados pelos(as)
participantes para se comunicarem com seus amigos(as) do Debian. 
</p>

<p>A equipe de publicidade do Debian fez sua usual «cobertura ao vivo» para
encorajar a participação, com micronotícias anunciando os diferentes eventos. A
equipe DebConf também forneceu diversas 
<a href="https://debconf21.debconf.org/schedule/mobile/">opções para seguir a programação em dispositivos móveis</a>.
</p>

<p>
Para quem não conseguiu participar, a maioria das palestras e sessões já está
disponível através do
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/">site web do acervo de eventos do Debian</a>,
e o restante ficará disponível nos próximos dias.
</p>

<p>
O site web da <a href="https://debconf21.debconf.org/">DebConf21</a>
permanecerá ativo por razões históricas e continuará a oferecer
os links para as apresentações e vídeos das palestras e eventos.
</p>

<p>
No próximo ano, a <a href="https://wiki.debian.org/DebConf/22">DebConf22</a>
está planejada para acontecer em Prizren, Kosovo, em julho de 2022.
</p>

<p>
A DebConf é comprometida com um ambiente seguro e acolhedor para todos(as) os(as) participantes.
Durante a conferência, diversos times (recepção, equipe de boas-vindas e equipe de comunidade)
estiveram disponíveis para auxiliar, de modo que os(as) participantes tivessem as melhores experiências
na conferência, e para buscar soluções para quaisquer problemas que pudessem surgir.
Veja a <a href="https://debconf21.debconf.org/about/coc/">página web sobre o código de conduta no site da DebConf21</a>
para mais detalhes.
</p>

<p>
O Debian agradece o compromisso de numerosos(as)
<a href="https://debconf21.debconf.org/sponsors/">patrocinadores(as)</a>
que apoiaram à DebConf21, particularmente nossos(as) patrocinadores(as)
Platinum:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://code4life.roche.com/">Roche</a>,
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>
e <a href="https://google.com/">Google</a>.
</p>

<h2>Sobre o Debian</h2>
<p>
O Projeto Debian foi fundado em 1993 por Ian Murdock para ser um verdadeiro
projeto comunitário e livre. Desde então, o projeto cresceu e tornou-se um dos
maiores e mais influentes projetos de código aberto. Milhares de
voluntários(as) de todos os cantos do mundo trabalham juntos(as) para criar e
manter o software Debian. Disponível em 70 idiomas, e com suporte para uma gama
imensa de tipos de computadores, o Debian se autodenomina o
<q>sistema operacional universal</q>.
</p>

<h2>Sobre a DebConf</h2>

<p>
A DebConf é a conferência dos(as) desenvolvedores(as) do Projeto Debian. Além de
uma completa programação de palestras técnicas, sociais e políticas, a DebConf
fornece uma oportunidade para desenvolvedores(as), contribuidores(as) e outras
pessoas interessadas se encontrarem presencialmente e trabalharem mais
proximamente. Ela acontece anualmente desde 2000 em locais tão variados como
Escócia, Argentina, Bósnia e Herzegóvina, e Brasil. Mais informações sobre a
DebConf estão disponíveis em
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Sobre a Lenovo</h2>
<p>
Como uma líder global em tecnologia, fabricando um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos de realidade aumentada/virtual, casas/escritórios inteligentes e
soluções para datacenters, a <a href="https://www.lenovo.com">Lenovo</a>
entende quão críticos são os sistemas abertos e as plataformas para um mundo
conectado.
</p>

<h2>Sobre a Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> é a maior empresa de
hospedagem web da Suíça, que também oferece serviços de backup e de
armazenamento, soluções para organizadores(as) de eventos, serviços de
transmissão em tempo real e vídeo sob demanda. Todos os datacenters e elementos
críticos para o funcionamento dos serviços e produtos oferecidos pela empresa
(tanto software quanto hardware) são de propriedade absoluta da Infomaniak.
</p>

<h2>Sobre a Roche</h2>
<p>
<a href="https://code4life.roche.com/">Roche</a> é um grande fornecedor
farmacêutico internacional e companhia de pesquisa, dedicada aos cuidados
personalizados com a saúde. Mais de 100.000 funcionários(as) ao redor do mundo
trabalham para resolver alguns dos maiores desafios da humanidade usando ciência
e tecnologia. A Roche está envolvida fortemente em projetos de pesquisa
colaborativos financiados publicamente com outros(as) parceiros(as) industriais
e acadêmicos(as), e apoia a DebConf desde 2017.
</p>

<h2>Sobre a Amazon Web Services (AWS)</h2>
<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> é uma das
plataformas em nuvem mais abrangentes e largamente adotadas no mundo,
oferecendo mais de 175 serviços repletos de funcionalidades via datacenters
globais (em 77 zonas de disponibilidade dentro de 24 regiões geográficas).
Alguns(mas) consumidores(as) da AWS incluem as startups de mais rápido
crescimento, as maiores empresas e as agências governamentais de liderança.
</p>

<h2>Sobre a Google</h2>
<p>
<a href="https://google.com/">Google</a>  é uma das maiores empresas de
tecnologia do mundo, fornecendo uma ampla gama de serviços e produtos
relacionados à Internet como tecnologias de publicidade on-line, pesquisa,
computação em nuvem, software e hardware.
</p>
<p>
A Google tem apoiado o Debian através do patrocínio da DebConf por mais de
dez anos, e é também uma parceira Debian patrocinando partes da infraestrutura
de integração contínua do <a href="https://salsa.debian.org">Salsa</a> dentro da
Plataforma em Nuvem da Google.
</p>

<h2>Informações para contato</h2>

<p>Para mais informações, por favor visite a página web da DebConf21 em
<a href="https://debconf21.debconf.org">https://debconf21.debconf.org</a>
ou envie um e-mail em inglês para &lt;press@debian.org&gt;.</p>
