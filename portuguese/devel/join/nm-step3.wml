#use wml::debian::template title="Etapa 3: filosofia e procedimentos" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="517d7a4f999561b8f28e207214a40990fdc3da49"

<p>As informações nesta página, embora públicas, são de interesse
principalmente dos(as) futuros(as) desenvolvedores(as) Debian.</p>

<h2>Etapa 3: filosofia e procedimentos</h2>

<h3>Filosofia</h3>
<p>
Espera-se que o(a) <a href="./newmaint#Applicant">candidato(a)</a> se encaixe
na comunidade Debian, que é construída em torno da filosofia do Software Livre.
O que o Debian entende como "livre" e como isso é aplicado, é explicado no
<a href="$(HOME)/social_contract">contrato social </a> e na
<a href="$(HOME)/social_contract#guidelines">definição Debian de software livre</a>.
<br />
O(A) futuro(a) desenvolvedor(a) precisa entender esses documentos
suficientemente bem para expressar as ideias e ideais descritos lá em suas
próprias palavras. Como esse entendimento é exatamente realizado e comunicado,
fica a critério do(a) candidato(a) e de seu(sua) gestor(a). Os métodos a seguir
servem apenas como sugestão, não como requisito, mas são exemplos de maneiras
de concluir esta etapa do processo. Várias oportunidades serão fornecidas para
o(a) candidato(a) mostrar entendimento nessas áreas.
</p>

<p>Nota: embora exijamos que o(a) candidato(a) concorde com a filosofia do
Debian, isso é limitado ao trabalho realizado para o Debian. Entendemos que as
pessoas precisam ganhar a vida e às vezes são obrigadas a trabalhar em projetos
não livres para seu(sua) empregador(a) ou cliente.</p>

<p>O Debian não tenta controlar o que o(a) candidato(a) pensa sobre esses
assuntos, mas é importante para a estabilidade de um projeto tão grande e
amorfo que todos(as) os(as) participantes trabalhem dentro do mesmo conjunto de
princípios e crenças básicas.</p>

<p>O <a href="./newmaint#AppMan">gestor(a) de candidatura (AM)</a>
decidirá quando os critérios para cada etapa foram atendidos. As diretrizes a
seguir tentam apenas fornecer exemplos úteis. Na maioria dos casos, uma mistura
de todos os critérios será usada.
<br>
O AM e o(a) <a href="./newmaint#Applicant">candidato(a)</a> podem decidir sobre
outras tarefas além das especificadas aqui. Essas tarefas devem ser
documentadas claramente no relatório final para o(a)
<a href="./newmaint#DAM">gestor(a) de contas Debian</a>.</p>

<dl>
 <dt>1. O <a href="$(HOME)/social_contract">contrato social</a></dt>
 <dd><p>
  O contrato social declara os objetivos e aspirações do Debian. Ele também
  tenta expressar nossas responsabilidades autoimpostas ao resto da comunidade.
  <br />
  Um entendimento adequado das prioridades que damos a essas várias
  responsabilidades, e estar de acordo com elas, são essenciais para qualquer
  candidato(a).
  </p>

  <p>O entendimento pode ser documentado de várias maneiras:</p>

  <ul>
   <li>Uma discussão com o AM sobre os vários termos do contrato social,
    expressando como eles se relacionam entre si e com a organização do
    Debian.</li>

   <li>Uma discussão sobre os objetivos pessoais do(a) candidato(a) no Debian,
    como eles se encaixam no contrato social, em alguns casos pode ser
    suficiente. </li>

   <li>O(A) candidato(a) pode descrever o contrato social com suas próprias
    palavras, explicando algumas das partes mais complexas e como o Debian se
    esforça para cumpri-las.<br />
    Nota: esta é a maneira geralmente escolhida.
   </li>
  </ul>
  <br>
 </dd>

 <dt>2. A <a href="$(HOME)/social_contract#guidelines">definição Debian de software livre (DFSG)</a></dt>
 <dd>
  <p> Esses princípios funcionam como diretrizes para determinar a liberdade
  oferecida por uma licença específica. </p>

  <p>Embora a maioria dos(as) candidatos(as) não sejam advogados(as), todos(as)
  devem ser capazes de expressar e usar o entendimento dos princípios básicos
  estabelecidos nestas diretrizes. </p>

  <p>O entendimento pode ser documentado de várias maneiras:</p>

  <ul>
   <li>O(A) candidato(a) discute várias licenças e tenta mostrar se elas são
    livres ou não. Nesse processo, a AM pode apontar casos especiais e fazer
    mais perguntas sobre a DFSG.<br>
    Nota: esta é a maneira geralmente escolhida.
   </li>

   <li>O(A) candidato(a) compara a definição debian de software livre com outras
    diretrizes sobre Software Livre e aponta semelhanças e diferenças.
#FIXME: "statements" is so wrong, but I already used guidelines...
   </li>
  </ul>
 </dd>
</dl>

<p>Qualquer que seja o método usado, o(a) candidato(a) deve concordar com esses
princípios, além de mostrar um entendimento do seu significado e conteúdo. </p>

<p>Não concordar com estes termos encerrará o processo de candidatura.</p>

<h3>Procedimentos</h3>

<p>Os procedimentos padrão e as políticas que evoluíram na criação do sistema
Debian são muito importantes para gerenciar o trabalho distribuído dos(as)
voluntários(as). Eles garantem a qualidade geral do Debian e geralmente ajudam
a evitar problemas entre os(as) desenvolvedores(as) fornecendo um conjunto de
diretrizes para a interação em casos especiais.</p>

<p>Como o(a) <a href="./newmaint#Applicant">candidato(a)</a> mostra seu
entendimento depende do(a) <a href="./newmaint#AppMan">gestor(a) de candidatura</a>,
mas há alguns itens essenciais que sempre devem ser abordados.
A lista a seguir documenta o que é obrigatório para as verificações de
procedimentos:</p>

<ul>
 <li><h4>Trabalhando com o sistema de acompanhamento de bugs</h4>
  O Debian usa o
  <a href="https://bugs.debian.org/">sistema de acompanhamento de bugs (BTS)</a>
  não apenas para rastrear bugs dos pacotes, mas também para coletar
  solicitações sobre a infraestrutura e gerenciar os pacotes que precisam de
  trabalho e os pacotes futuros.
  <br />
  Os(As) futuros(as) desenvolvedores(as) precisam ser capazes de manusear o
  BTS e explicar como ele pode ser usado para representar todos os dados
  disponíveis sobre problemas.
 </li>

 <li><h4>O processo de lançamento do Debian</h4>
  O processo de lançamento do Debian é a base para sua estabilidade e segurança,
  portanto os(as) futuros(as) desenvolvedores(as) precisam entender como ele
  funciona, por que está estruturado como está e quais exceções são possíveis.
 </li>

 <li><h4>Esforços de internacionalização e localização do Debian</h4>
  Considerando que apenas uma pequena parte do mundo fala inglês nativamente,
  desenvolvedores(as) e tradutores(as) investem uma quantidade significativa de
  tempo para tornar o Debian utilizável para todos(as). Existem muitas
  ferramentas específicas e regras, e os(as) futuros(as) desenvolvedores(as)
  devem estar cientes deles.
 </li>
</ul>

<p>É claro que existem muitos outros tópicos que podem ser cobertos pelas
verificações de novos(as) membros(as), mas o AM deve escolher apenas aqueles
que são relevantes para a área em que o(a) candidato(a) deseja trabalhar.
A qualidade mais importante é que o(a) futuro(a) desenvolvedor(a) saiba
onde procurar informações sobre eles.</p>

<p>O(A) <a href="./newmaint#Applicant">candidato(a)</a> deve também ler a
<a href="$(DEVEL)/dmup">política de uso de máquinas do Debian (DMUP)</a>
e concordar em cumpri-la.</p>

<hr />
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
