<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An XXE vulnerability was found in Nokogiri, a Rubygem providing HTML, XML, SAX,
and Reader parsers with XPath and CSS selector support.</p>

<p>XML Schemas parsed by Nokogiri::XML::Schema were trusted by default, allowing
external resources to be accessed over the network, potentially enabling XXE or
SSRF attacks. The new default behavior is to treat all input as untrusted.
The upstream advisory provides further information how to mitigate the problem
or restore the old behavior again.</p>

<p><a rel="nofollow" href="https://github.com/sparklemotion/nokogiri/security/advisories/GHSA-vr8q-g5c7-m54m">https://github.com/sparklemotion/nokogiri/security/advisories/GHSA-vr8q-g5c7-m54m</a></p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.6.8.1-1+deb9u1.</p>

<p>We recommend that you upgrade your ruby-nokogiri packages.</p>

<p>For the detailed security status of ruby-nokogiri please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ruby-nokogiri">https://security-tracker.debian.org/tracker/ruby-nokogiri</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2678.data"
# $Id: $
