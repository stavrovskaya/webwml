<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Ceph, a distributed storage
and file system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14662">CVE-2018-14662</a>

     <p>Authenticated ceph users with read only permissions could steal dm-crypt
     encryption keys used in ceph disk encryption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16846">CVE-2018-16846</a>

     <p>Authenticated ceph RGW users can cause a denial of service against OMAPs
     holding bucket indices.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10753">CVE-2020-10753</a>

     <p>A flaw was found in the Red Hat Ceph Storage RadosGW (Ceph Object
     Gateway).
     The vulnerability is related to the injection of HTTP headers via a CORS
     ExposeHeader tag. The newline character in the ExposeHeader tag in the
     CORS configuration file generates a header injection in the response when
     the CORS request is made.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1760">CVE-2020-1760</a>

     <p>A flaw was found in the Ceph Object Gateway, where it supports request
     sent by an anonymous user in Amazon S3. This flaw could lead to potential
     XSS attacks due to the lack of proper neutralization of untrusted input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3524">CVE-2021-3524</a>

     <p>A flaw was found in the Red Hat Ceph Storage RadosGW (Ceph Object Gateway)
     The vulnerability is related to the injection of HTTP headers via a CORS
     ExposeHeader tag. The newline character in the ExposeHeader tag in the
     CORS configuration file generates a header injection in the response when
     the CORS request is made. In addition, the prior bug fix for CVE-2020     10753 did not account for the use of \r as a header separator, thus a new
     flaw has been created.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
10.2.11-2+deb9u1.</p>

<p>We recommend that you upgrade your ceph packages.</p>

<p>For the detailed security status of ceph please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ceph">https://security-tracker.debian.org/tracker/ceph</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2735.data"
# $Id: $
