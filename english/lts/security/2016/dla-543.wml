<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that sqlite3, a C library that implements a SQL
database engine, would reject a temporary directory (e.g., as specified
by the TMPDIR environment variable) to which the executing user did not
have read permissions.  This could result in information leakage as less
secure global temporary directories (e.g., /var/tmp or /tmp) would be
used instead.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
3.7.13-1+deb7u3.</p>

<p>We recommend that you upgrade your sqlite3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-543.data"
# $Id: $
