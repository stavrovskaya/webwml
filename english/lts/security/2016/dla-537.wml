<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Roundcube, a webmail solution for IMAP servers, was susceptible to
cross-site-scripting (XSS) vulnerabilities when handling SVG images.
When right-clicking on the download link of an attached image, it was
possible that embedded Javascript could be executed in a separate Tab.</p>

<p>The update disables displaying of SVG images in e-mails and TABS.
Downloading attachments is still possible. This security update also
mitigates against other ways to exploit this issue in SVG images.
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-4068">CVE-2016-4068</a>)</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.7.2-9+deb7u3.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-537.data"
# $Id: $
