<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17091">CVE-2017-17091</a>

    <p>wp-admin/user-new.php in WordPress sets the newbloguser
    key to a string that can be directly derived from the user ID, which
    allows remote attackers to bypass intended access restrictions by
    entering this string.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17092">CVE-2017-17092</a>

    <p>wp-includes/functions.php in WordPress does not require the
    unfiltered_html capability for upload of .js files, which might
    allow remote attackers to conduct XSS attacks via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17093">CVE-2017-17093</a>

    <p>wp-includes/general-template.php in WordPress does not properly
    restrict the lang attribute of an HTML element, which might allow
    attackers to conduct XSS attacks via the language setting of a site.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17094">CVE-2017-17094</a>

    <p>wp-includes/feed.php in WordPress does not properly
    restrict enclosures in RSS and Atom fields, which might allow
    attackers to conduct XSS attacks via a crafted URL.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u20.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1216.data"
# $Id: $
