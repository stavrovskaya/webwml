<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A bug in the error handling of the send file code for the NIO HTTP
connector resulted in the current Processor object being added to the
Processor cache multiple times. This in turn meant that the same
Processor could be used for concurrent requests. Sharing a Processor
can result in information leakage between requests including, not
limited to, session ID and the response body.</p>

<p>In addition this update also addresses a regression when running
Tomcat 7 with SecurityManager enabled due to an incomplete fix for
<a href="https://security-tracker.debian.org/tracker/CVE-2016-6816">CVE-2016-6816</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u9.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-779.data"
# $Id: $
