<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>tcp_emu in tcp_subr.c in libslirp 4.1.0, as used in QEMU 4.2.0, mismanages
memory, as demonstrated by IRC DCC commands in EMU_IRC.
This can cause a heap-based buffer overflow or other out-of-bounds access
which can lead to a DoS or potential execute arbitrary code.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.1+dfsg-12+deb8u13.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2090.data"
# $Id: $
