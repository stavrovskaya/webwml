<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in sqlite3, a C library that
implements an SQL database engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8740">CVE-2018-8740</a>

    <p>Databases whose schema is corrupted using a CREATE TABLE AS statement
    could cause a NULL pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20346">CVE-2018-20346</a>

    <p>When the FTS3 extension is enabled, sqlite3 encounters an integer
    overflow (and resultant buffer overflow) for FTS3 queries that occur
    after crafted changes to FTS3 shadow tables, allowing remote
    attackers to execute arbitrary code by leveraging the ability to run
    arbitrary SQL statements.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20506">CVE-2018-20506</a>

    <p>When the FTS3 extension is enabled, sqlite3 encounters an integer
    overflow (and resultant buffer overflow) for FTS3 queries in a
    <q>merge</q> operation that occurs after crafted changes to FTS3 shadow
    tables, allowing remote attackers to execute arbitrary code by
    leveraging the ability to run arbitrary SQL statements</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5827">CVE-2019-5827</a>

    <p>Integer overflow allowed a remote attacker to potentially exploit
    heap corruption via a crafted HTML page, primarily impacting
    chromium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9936">CVE-2019-9936</a>

    <p>Running fts5 prefix queries inside a transaction could trigger a
    heap-based buffer over-read, which may lead to an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9937">CVE-2019-9937</a>

    <p>Interleaving reads and writes in a single transaction with an fts5
    virtual table will lead to a NULL Pointer Dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16168">CVE-2019-16168</a>

    <p>A browser or other application can be triggered to crash because of
    inadequate parameter validation which could lead to a divide-by-zero
    error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20218">CVE-2019-20218</a>

    <p>WITH stack unwinding proceeds even after a parsing error, resulting
    in a possible application crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13630">CVE-2020-13630</a>

    <p>The code related to the snippet feature exhibits a use-after-free
    defect.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13632">CVE-2020-13632</a>

    <p>A crafted matchinfo() query can lead to a NULL pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13871">CVE-2020-13871</a>

    <p>The parse tree rewrite for window functions is too late, leading to
    a use-after-free defect.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11655">CVE-2020-11655</a>

    <p>An improper initialization of AggInfo objects allows attackers to
    cause a denial of service (segmentation fault) via a malformed
    window-function query.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13434">CVE-2020-13434</a>

    <p>The code in sqlite3_str_vappendf in printf.c contains an integer
    overflow defect.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.16.2-5+deb9u2.</p>

<p>We recommend that you upgrade your sqlite3 packages.</p>

<p>For the detailed security status of sqlite3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sqlite3">https://security-tracker.debian.org/tracker/sqlite3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2340.data"
# $Id: $
