<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A heap-based buffer overflow in _cairo_image_surface_create_from_jpeg() in
extensions/cairo_io/cairo-image-surface-jpeg.c in gThumb and Pix allows
attackers to cause a crash and potentially execute arbitrary code via a crafted
JPEG file.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3:3.3.1-2.1+deb8u2.</p>

<p>We recommend that you upgrade your gthumb packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2066.data"
# $Id: $
