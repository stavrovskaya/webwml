<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two remote code execution
vulnerabilities in php-horde-image, the image processing library for the
Horde <url "https://www.horde.org/"> groupware tool:</p>

<ul>

 <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9774">CVE-2017-9774</a>
    <p>A remote code execution vulnerability (RCE) that was
   exploitable by a logged-in user sending a maliciously crafted HTTP GET
   request to various image backends.</p>

   <p>Note that the fix applied upstream has a regression in that it ignores
   the <q>force aspect ratio</q> option; see <a href="https://github.com/horde/Image/pull/1">https://github.com/horde/Image/pull/1</a>.</p></li>

 <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14650">CVE-2017-14650</a>
    <p>Another RCE that was exploitable by a logged-in
   user sending a maliciously crafted GET request specifically to the <q>im</q>
   image backend.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these issues have been fixed in php-horde-image
version 2.1.0-4+deb8u1.</p>

<p>We recommend that you upgrade your php-horde-image packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1395.data"
# $Id: $
