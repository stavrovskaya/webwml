<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in the MariaDB database server. The
vulnerabilities are addressed by upgrading MariaDB to the new upstream
version 10.0.38. Please see the MariaDB 10.0 Release Notes for further
details:</p>

<p><url "https://mariadb.com/kb/en/mariadb/mariadb-10038-release-notes/"></p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
10.0.38-0+deb8u1.</p>

<p>We recommend that you upgrade your mariadb-10.0 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1655.data"
# $Id: $
