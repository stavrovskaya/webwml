<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Samuel Gross discovered a type confusion bug in the JavaScript engine of
the Mozilla Firefox web browser, which could result in the execution of
arbitrary code when browsing a malicious website.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
60.7.1esr-1~deb8u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1829.data"
# $Id: $
