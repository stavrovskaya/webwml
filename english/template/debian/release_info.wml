#use wml::debian::common_tags
#use wml::debian::ctime

### WARNING ## WARNING ## WARNING ## WARNING ## WARNING ## WARNING ## WARNING ###
###                                                                           ###
### If you modify this file, do not push to mirrors the website until the     ###
### installation images are actually ready for download.                      ###
### This applies to all sections of this template.                            ###
### All sections may be updated at the same time.                             ###
### For release and point release announcement, follow the instructions in    ###
### https://wiki.debian.org/Teams/Publicity/ReleasePointAnnouncements         ###
### to see how to stop the website jobs and prepare the website without       ###
### pushing it to mirrors, and what to do when the images are ready.          ###
###                                                                           ###
### WARNING ## WARNING ## WARNING ## WARNING ## WARNING ## WARNING ## WARNING ###


<define-tag current_release_name>bullseye</define-tag>
<define-tag current_testing_name>bookworm</define-tag>
<define-tag current_oldstable_name>buster</define-tag>

<define-tag current_release><current_release_bullseye/></define-tag>
<define-tag current_release_date><current_release_date_bullseye/></define-tag>
# The following needs to point to the initial release version, like "11.0" for Bullseye (not "11")!
<define-tag current_initial_release>11.0</define-tag>
<define-tag current_initial_release_date><:=spokendate('2021-08-14'):></define-tag>

<define-tag current_release_security_name><current_release_name><if <gt <current_initial_release> 10 /> -security /updates /></define-tag>

<define-tag current_release_potato>2.2r7</define-tag>
<define-tag current_release_date_potato><:=spokendate('2002-07-13'):></define-tag>
<define-tag current_release_newsurl_potato>2002/20020713</define-tag>
<define-tag current_release_woody>3.0r6</define-tag>
<define-tag current_release_date_woody><:=spokendate('2005-06-02'):></define-tag>
<define-tag current_release_newsurl_woody>2005/20050602</define-tag>
<define-tag current_release_sarge>3.1r8</define-tag>
<define-tag current_release_date_sarge><:=spokendate('2008-04-13'):></define-tag>
<define-tag current_release_newsurl_sarge>2008/20080413</define-tag>
<define-tag current_release_etch>4.0r9</define-tag>
<define-tag current_release_date_etch><:=spokendate('2010-05-22'):></define-tag>
<define-tag current_release_newsurl_etch>2010/20100522</define-tag>
<define-tag current_release_lenny>5.0.10</define-tag>
<define-tag current_release_date_lenny><:=spokendate('2012-03-10'):></define-tag>
<define-tag current_release_newsurl_lenny>2012/20120310</define-tag>
<define-tag current_release_squeeze>6.0.10</define-tag>
<define-tag current_release_date_squeeze><:=spokendate('2014-07-19'):></define-tag>
<define-tag current_release_newsurl_squeeze>2014/20140719</define-tag>
<define-tag current_release_wheezy>7.11</define-tag>
<define-tag current_release_date_wheezy><:=spokendate('2016-06-04'):></define-tag>
<define-tag current_release_newsurl_wheezy>2016/2016060402</define-tag>
<define-tag current_release_jessie>8.11</define-tag>
<define-tag current_release_date_jessie><:=spokendate('2018-06-23'):></define-tag>
<define-tag current_release_newsurl_jessie>2018/20180623</define-tag>
<define-tag current_release_stretch>9.13</define-tag>
<define-tag current_release_date_stretch><:=spokendate('2020-07-18'):></define-tag>
<define-tag current_release_newsurl_stretch>2020/20200718</define-tag>
<define-tag current_release_buster>10.10</define-tag>
<define-tag current_release_date_buster><:=spokendate('2021-06-19'):></define-tag>
<define-tag current_release_newsurl_buster>2021/20210619</define-tag>
<define-tag current_release_bullseye>11.0</define-tag>
<define-tag current_release_date_bullseye><:=spokendate('2021-08-14'):></define-tag>
<define-tag current_release_newsurl_bullseye>2021/20210814</define-tag>
<define-tag current_release_bookworm>12.0</define-tag>
<define-tag current_release_date_bookworm>TBA</define-tag>
<define-tag current_release_newsurl_bookworm>TBA</define-tag>
<define-tag current_release_trixie></define-tag>
<define-tag current_release_date_trixie>TBA</define-tag>
<define-tag current_release_newsurl_trixie></define-tag>

<define-tag current_release_short><: $f = "<current_release/>"; $f =~ s/r\d+$//; $f =~ s/^(\d\.\d)\.\d$/$1/; print $f :></define-tag>

<define-tag current-cd-release>11.0.0</define-tag>
<define-tag current-cd-release-dirname>current</define-tag>
# netinst and buisnesscard name
<define-tag current-tiny-cd-release-filename><current-cd-release/></define-tag>
# live CD are not always in sync
<define-tag current-live-cd-release-dirname>current</define-tag>
# non-free cd including firmware are not always in sync
<define-tag current-nonfree-cd-release-dirname><current-cd-release/>+nonfree</define-tag>
<define-tag current-nonfree-cd-release-filename><current-cd-release/></define-tag>

<define-tag packages_in_stable>59000</define-tag>

<set-var diversions:_cnt=0 />

<define-tag if-stable-release endtag=required>
<preserve release />
<set-var %attributes />
<ifneq "<current_release_name />" "<get-var release />"
	      "<enter NO<get-var release /><get-var diversions:_cnt /> />" "<enter <get-var release /><get-var diversions:_cnt /> />" />
%body
<leave />
<dump "<get-var release /><get-var diversions:_cnt />" />
<increment diversions:_cnt />
<restore release />
</define-tag>
