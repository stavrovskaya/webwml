#use wml::debian::template title="Debian GNU/Linux 2.2 ('potato') Release-informatie" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/potato/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="552f9232b5d467c677963540a5a4236c824e87b9"

<p>Debian GNU/Linux 2.2 (alias Potato) is uitgebracht op
<:=spokendate ("14-08-2000"):>. De laatste tussenrelease van Debian 2.2 is
<current_release_potato>, uitgebracht op <a href="$(HOME)/News/<current_release_newsurl_potato/>"><current_release_date_potato></a>.</p>

<p><strong>Debian GNU/Linux 2.2 is achterhaald door
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a>.
Beveiligingsupdates werden stopgezet op 30 juni 2003.</strong>
Raadpleeg
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
de resultaten van de enquête van het beveiligingsteam</a> voor meer informatie.</p>

<p>Voor informatie over de belangrijkste wijzigingen in deze release, kunt u
de <a href="releasenotes">Notities bij de release</a> en het officiële
<a href="$(HOME)/News/2000/20000815">persbericht</a> raadplegen.</p>

<p>Debian GNU/Linux 2.2 is opgedragen aan de nagedachtenis van Joel "Espy" Klecker,
een ontwikkelaar van Debian, die zonder medeweten van het grootste deel van
het Debian-project tijdens het grootste deel van zijn betrokkenheid bij
Debian, bedlegerig was en vocht tegen een ziekte welke bekend staat als
spierdystrofie van Duchenne. Pas nu realiseert het Debian-project zich de
omvang van zijn toewijding en de vriendschap die hij ons schonk. Daarom
werd als blijk van waardering en ter herinnering aan zijn inspirerende
leven, deze release van Debian GNU/Linux aan hem opgedragen.</p>

<p>Debian GNU/Linux 2.2 is beschikbaar via het Internet of bij CD-verkopers.
Raadpleeg de <a href="$(HOME)/distrib/">Distributiepagina</a>  voor meer
informatie over het verkrijgen van Debian.</p>

<p>De volgende architecturen worden in deze release ondersteund:</p>

<ul>
<: foreach $arch (@arches) {
      print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
   } :>
</ul>

<p>Voor u Debian gaat installeren, leest u best de <A HREF="installmanual">\
Installatiehandleiding</A>. De Installatiehandleiding voor uw
doelarchitectuur bevat instructies en links voor alle bestanden die u
voor de installatie nodig heeft. Wellicht bent u ook geïnteresseerd in de
<a href="installguide/">installatiegids van Debian 2.2</a>, een online tutorial.</p>

<p>Indien u APT gebruikt, kunt u de volgende regels gebruiken in uw bestand
<code>/etc/apt/sources.list</code> om toegang te krijgen tot de pakketten
van potato:</p>

<pre>
  deb http://archive.debian.org potato main contrib non-free
  deb http://non-us.debian.org/debian-non-US potato/non-US main non-free
</pre>

<p>Lees de man-pagina's <code>apt-get</code>(8) en <code>sources.list</code>(5)
voor meer informatie.</p>

<p>In tegenstelling tot wat we wensen, bevat de potato-release enkele
problemen, ook al werd deze <em>stabiel</em> verklaard. We maakten
<a href="errata">een lijst op van de belangrijkste bekende problemen</a> en
u kunt ons steeds nog <a href="reportingbugs">andere problemen rapporteren</a>.</p>

<p>Gegevensintegriteit wordt verkregen door een digitaal ondertekend
<code>Release</code>-bestand. Om ervoor te zorgen dat alle bestanden in de
release daar wel degelijk toe behoren, worden de
MD5-controlesommen van alle <code>Packages</code>-bestanden gekopieerd
naar het <code>Release</code>-bestand. Digitale handtekeningen voor
dit bestand zijn met de volgende sleutel opgeslagen in het bestand
<code>Release.gpg</code>: <a
href="https://ftp-master.debian.org/ziyi_key_2002.asc">\
ftpmaster</a>.</p>

<p>Om het <code>Release</code>-bestand te verifiëren moet u beide bestanden
ophalen en de opdracht <code>gpg --verify Release.gpg Release</code>
uitvoeren nadat u de sleutels waarmee het ondertekend werd, geïmporteerd heeft.</p>

<p>Tenslotte hebben we een lijst van <a href="credits">mensen die bijdroegen</a> tot de realisatie van deze release.</p>
