#use wml::debian::template title="Woonplaatsen van ontwikkelaars"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<p>Veel mensen hebben interesse getoond in informatie over de
geografische spreiding van Debian ontwikkelaars.
Daarom besloten we aan onze database van ontwikkelaars een veld
toe te voegen, waar ontwikkelaars hun wereldcoördinaten
kunnen invoeren.

<p>De onderstaande kaart is gemaakt met het programma
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a> op basis van een geanonimiseerde
<a href="developers.coords">lijst van coördinaten van ontwikkelaars</a>.

<p><img src="developers.map.jpeg" alt="Wereldkaart">

<p>Indien u een ontwikkelaar bent, kunt u desgewenst uw coördinaten
toevoegen aan uw gegevens in onze
<a href="https://db.debian.org">Database van Debian ontwikkelaars</a>.
U kunt op de database inloggen en er uw gegevens wijzigen.
Indien u de coördinaten van uw woonplaats niet kent, zou u ze moeten
kunnen opzoeken via de volgende website:
<ul>
<li><a href="https://osm.org">Openstreetmap</a>
Zoek in de zoekbalk uw woonplaats op.
Selecteer de richtingspijlen naast de zoekbalk. Sleep daarna de groene
marker naar de OSM-kaart en in het vak 'van' verschijnen de coördinaten.
</ul>

<p>De opmaak van de coördinaten mag één van de drie volgende
   vormen hebben:
<dl>
<dt>Decimale graden
<dd>De opmaak is dan +-DDD.DDDDDDDDDDDDDDD. Dit is een opmaak
    die gebruikt wordt door programma's zoals xearth en
    die ook op veel websites voor positiebepaling
    in gebruik is. De precisie ervan blijft evenwel beperkt
    tot 4 of 5 decimalen.
<dt>Graden Minuten (DGM)
<dd>De opmaak ervan is +-DDDMM.MMMMMMMMMMMMM. Het is geen
    rekenkundige, maar een gecomprimeerde weergave
    van twee afzonderlijke eenheden, graden en minuten.
    Deze schrijfwijze is gangbaar bij sommige types fiets-
    en wandel-gps-toestellen en bij berichten in het
    NMEA-protocol.
<dt>Graden Minuten Seconden (DGMS)
<dd>De opmaak is in dit geval +-DDDMMSS.SSSSSSSSSSS. Net zoals
	dit voor DGM het geval is, gaat het niet om een rekenkundige
	voorstelling, maar om een gecomprimeerde weergave van drie
    aparte eenheden: graden, minuten en seconden. Deze schrijfwijze
    is vooral gebruikelijk op websites die voor elke positie drie
    waarden opgeven. Bijvoorbeeld 34:50:12.24523 N (noord)
    is zo een positie, die overeenkomstig DGMS op de
    volgende wijze zal geschreven worden: +0345012.24523.
</dl>

<p>
Bij breedtegraden staat + voor noorderbreedte. In het geval van
lengtegraden staat + voor oosterlengte. Indien uw positie minder
dan 2 graden verwijderd is van een nulpunt, is het van belang om
voldoende voorloopnullen te gebruiken opdat duidelijk zou zijn
welke opmaak van toepassing is.
