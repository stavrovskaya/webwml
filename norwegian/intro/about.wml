#use wml::debian::template title="Om Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="9f7c29520cbbd185a4c5c73955b45f83eaf6f589" maintainer="Hans F. Nordhaug"
# Opprinnelig oversatt til norsk av Tor Slettnes (tor@slett.net)
# Vedlikeholdt av Hans F. Nordhaug <hansfn@gmail.com>

  <ul class="toc">
    <li><a href="#what">HVA er Debian?</a></li>
    <li><a href="#free">Er alt dette fritt tilgjengelig?</a></li>
    <li><a href="#CD">Du sier gratis, men CDer/båndbredde koster penger.</a></li>
    <li><a href="#disbelief">De fleste programvarene koster tusenvis.
	Hvordan kan du gi det bort?</a></li>
    <li><a href="#hardware">Hvilken maskinvare støttes?</a></li>
    <li><a href="#info">Før jeg bestemmer meg, trenger jeg mer informasjon.</a></li>
    <li><a href="#why">Jeg er forsatt ikke overbevist.
	Hva er noen fordeler og ulemper med Debian?</a></li>
    <li><a href="#install">Hvordan får jeg tak i Debian?</a></li>
    <li><a href="#support">Jeg klarer ikke å sette alt opp selv.
	Hvor får jeg brukerstøtte for Debian?</a></li>
    <li><a href="#who">Hvem er dere egentlig?</a></li>
    <li><a href="#users">Hvem bruker Debian?</a></li>
    <li><a href="#history">Hvordan startet det hele?</a></li>
  </ul>

#include "about_sub_what.data"

#include "about_sub_free.data"

  <h2><a name="CD">Dere sier fritt tilgjengelig, men CDer og
      båndbredde koster penger!</a></h2>

  <p>
    Du spør kanskje: Om programvaren er fritt tilgjengelig, hvorfor
    må jeg betale for en CD, eller en Internettleverandør for
    nedlasting?</p>

  <p>
    Når du kjøper en CD, betaler du for noens tid, pengeutlegg for å
    lage disken, og risiko (kanskje selger de ingen CD-er).  Med andre
    ord betaler du for det fysisk mediumet som brukes til å levere
    programvaren, og ikke for selve programvaren.</p>

  <h2><a name="disbelief">De fleste programvarene koster tusenvis.
      Hvordan kan du gi det bort?</a></h2>

  <p>
    Et bedre spørsmål er hvordan programvarebedrifter slipper unna
    med å forlange så mye.  Å lage programvare er ikke som å lage en
    bil.  Etter at du har laget en kopi av programvaren, er
    produksjonskostnadene ved å lage en million flere bittesmå (det er
    en grunn til at Microsoft har så mange milliarder dollar i
    banken).</p>

  <p>
    Se på det på en annen måte: om du hadde en uendelig mengde sand
    tilgjengelig i hagen din, ville du sikkert være villig til å gi bort
    sand.  Det ville likevel være tåpelig å betale for en lastebil til å
    frakte den til andre.  Du ville fått dem til å komme å hente den
    selv (på samme måten som du kan hente fra nettet), eller de kan
    betale noen andre for å levere den til døren sin (som motsvarer til
    å kjøpe en cd).  Dette er nøyaktig hvordan Debian opererer, og
    grunnen til at de fleste cd-ene er så billige (mindre enn en
    femtilapp for tre cd-er).</p>

  <p>
    Debian tjener ingen penger på salg av CDer.  Samtidig trenger vi
    penger for å betale utgifter som domeneregistrasjon og maskinvare.
    Derfor ber vi om at du gjerne kjøper fra en av de
    <a href="../CD/vendors/">cd-leverandørene</a> som 
    <a href="$(HOME)/donations">donerer</a> en del av salget ditt til
    Debian.</p>

  <h2><a name="hardware">Hvilken maskinvare støttes?</a></h2>

  <p>
    Debian kan kjøre på nesten alle personlige datamaskiner, inkludert
    de fleste eldre modeller. Hver ny utgave av Debian støtter stort sett flere
    maskinarkitekturer.  For en fullstendig liste over arkitekturer
    som støttes nå, se <a href="../releases/stable/">dokumentasjonen
    for den stabile utgaven</a>.</p>

  <p>
    Nesten all vanlig maskinvare støttes. Om du vil forsikre deg at alle
    enheter koblet til maskinen din er støttet, sjekk wiki-siden
    <a href="https://wiki.debian.org/InstallingDebianOn/">DebianOn</a>.</p>

  <p>
    Det fins noen bedrifter som gjør det vanskelig å støtte
    maskinvarene sin på grunn av at de ikke utgir spesifikasjoner for
    dem.  Dette betyr at du kanskje ikke vil kunne bruke disse
    maskinvarene med GNU/Linux.  Noen bedrifter tilbyr et ufrie
    grensesnitt mot maskinvarene sine, men dette er problematisk
    fordi bedriften kan muligvis opphøre eller vil kanskje ikke lenger
    støtte maskinvaren du har i framtiden.  Vi anbefaler at du kun
    kjøper maskinvare fra leverandører som tilbyr <a href="free">fritt
    tilgjengelig</a> maskingrensesnitt/driverutiner.</p>


 <h2><a name="info">Jeg leter etter mer informasjon.</a></h2>

  <p>
    Se gjerne på listen vår over <a href="$(DOC)/manuals/debian-faq/">vanlige
      spørsmål</a> (FAQ).</p>

  <h2><a name="why">Jeg er forsatt ikke overbevist.</a></h2>

  <p>
    Ta ikke vårt ord for det - prøv Debian selv.  I og med at
    harddisk-lagre er blitt rimeligere, kan du sikkert finne fram
    500MB eller så.  Debian kan lettvint bli installert på en slik
    plass, og kan leve ved siden av dine nåværende operativsystemer.
    Dersom du trenger mer plass, kan du rett og slett fjerne ett av
    disse operativsystemene (og etter at du ser handlekraften til
    Linux, er vi temmelig sikker på at det ikke vil bli Debian).</p>

  <p>
    Ettersom utprøving av et nytt operativsystem vil ta en del av den
    dyre tiden din, er det forståelig om du har noen reservasjoner.  Av
    denne grunnen har vi samlet en liste av 
    <a href="why_debian">fordeler og ulemper med Debian</a>.  Dette burde
    hjelpe deg å avgjøre om du syns det er verdt forsøket.  Vi håper du
    setter pris på vår ærlige og oppriktige meninger.</p>


  <h2><a name="install">Hvordan får jeg tak i Debian?</a></h2>

    <p>
      Den mest populære måten å installere Debian på er fra en CD, som
      du kan kjøpe for kostnadspris hos en av våre mange
      CD-leverandører.  Om du har en god Internett-tilknytning, kan du
      hente og installere Debian via Internettet.</p>

    <p>
      Se <a href="../distrib/">siden om å få tak i Debian</a> for mer
      informasjon.</p>

  <h2><a name="support">Jeg klarer ikke å sette alt opp selv.
      Hvor får jeg brukerstøtte for Debian?</a></h2>

    <p>
      Du kan få mye hjelp ved å lese dokumentasjon som er tilgjengelig
      både på dette nettstedet og i pakker du kan installere på
      systemet ditt.  Du kan også ta kontakt med oss på postlistene og
      ved å bruke IRC.  Du kan til og med hyre en konsulent til å
      utføre arbeidet.</p>

    <p>
      Se <a href="../doc/">dokumentasjonen</a> og
      <a href="../support">støttesidene</a> for videre
      informasjon.</p>
    

  <h2><a name="who">Hvem er dere egentlig?</a></h2>

#include "about_sub_who.data"

    <p>
      Den komplette listen med offisielle Debian-medlemmer fins på
      <a href="https://nm.debian.org/members">nm.debian.org</a>, hvor 
      medlemskap håndteres. En bredere liste med bidragsytere til Debian fins på
      <a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

  <h2><a name="users">Hvem bruker Debian?</a></h2>

    <p>
      Selvom det ikke finnes nøyaktige statistikker (siden Debian ikke 
      krever at brukerne må registrere seg), er der sterke beviser for 
      at Debian brukes av et bredt spekter av organisasjoner, store og små, 
      utenom mange tusenvis private brukere. Se vår side om 
      <a href="../users/">hvem som bruker Debian</a> for en liste over 
      kjente organisasjoner som har sendt inn korte beskrivelser av
      hvordan og hvorfor de bruker Debian.</p>

  <h2><a name="history">Hvordan startet det hele?</a></h2>

#include "about_sub_history.data"
