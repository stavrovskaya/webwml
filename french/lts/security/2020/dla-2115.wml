#use wml::debian::translation-check translation="d76d9b8fd92dcb932fc3a4569d46eea4fa2527a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité d’utilisation de mémoire
après libération dans le serveur FTP <tt>proftpd-dfsg</tt>.</p>

<p>L’exploitation de cette vulnérabilité à l’intérieur de la gestion de mémoire
mutuelle pouvait permettre à un attaquant distant d’exécuter du code
sur le système touché.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9273">CVE-2020-9273</a>

<p>Dans ProFTPD 1.3.7, il était possible d’altérer la mémoire mutuelle en
interrompant le canal de transfert de données. Cela provoque une utilisation de
mémoire après libération dans <tt>alloc_pool</tt> dans <tt>pool.c</tt>, et
une exécution possible de code à distance.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.5e+r1.3.5-2+deb8u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets proftpd-dfsg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2115.data"
# $Id: $
