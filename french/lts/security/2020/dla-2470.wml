#use wml::debian::translation-check translation="5e34db70bb9e758f6b8446f514ea1daa00d48b47" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>                 <p><a href="https://security-tracker.debian.org/tracker/CVE-2018-1100">CVE-2018-1100</a> <a href="https://security-tracker.debian.org/tracker/CVE-2018-13259">CVE-2018-13259</a> <a href="https://security-tracker.debian.org/tracker/CVE-2019-20044">CVE-2019-20044</a> Bogues Debian : 908000 894044 894043 895225 951458</p>
<p>Plusieurs vulnérabilités de sécurité ont été trouvées et corrigées dans zsh,
un langage de script et un interpréteur de commandes puissant. Des erreurs dues
à un décalage d'entier, une mauvaise analyse de la ligne de shebang et des
dépassements de tampon pourraient conduire à un comportement inattendu. Un
utilisateur local non privilégié peut créer un fichier de message ou un chemin
de répertoire contrefait spécialement. Si un utilisateur l’accepte et est
privilégié ou traverse ce chemin, cela aboutit à une élévation des privilèges.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.3.1-4+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zsh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zsh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zsh">https://security-tracker.debian.org/tracker/zsh</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2470.data"
# $Id: $
