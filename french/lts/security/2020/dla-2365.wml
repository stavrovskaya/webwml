#use wml::debian::translation-check translation="5760135f984834fada6d66960163893f950ff71b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans netty-3.9, un cadriciel
client/serveur NIO de socket en Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16869">CVE-2019-16869</a>

<p>Netty avant 4.1.42.Final gère incorrectement un espace blanc avant le
deux-points dans les en-têtes HTTP (tel qu’une ligne « Transfer-Encoding :
chunked »), qui conduit à une dissimulation de requête HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20444">CVE-2019-20444</a>

<p>HttpObjectDecoder.java dans Netty avant la version 4.1.44 permet un en-tête
HTTP auquel manque un deux-points, qui pourrait être interprété comme un en-tête
indépendant avec une syntaxe incorrecte, ou qui pourrait être interprété comme un
«invalid fold».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20445">CVE-2019-20445</a>

<p>HttpObjectDecoder.java dans Netty avant la version 4.1.44 permet un en-tête
Content-Length accompagné par un autre en-tête Content-Length, ou par un en-tête
Transfer-Encoding.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.9.9.Final-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netty-3.9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de netty-3.9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/netty-3.9">https://security-tracker.debian.org/tracker/netty-3.9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2365.data"
# $Id: $
