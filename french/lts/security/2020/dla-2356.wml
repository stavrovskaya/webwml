#use wml::debian::translation-check translation="7627f353ed3c540adb08648921798cec349472c8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été signalées à l’encontre de FreeRDP, une mise
en œuvre au code source ouvert de serveur et de client au protocole RDP de
Microsoft.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0791">CVE-2014-0791</a>

<p>Un dépassement d'entier dans la fonction license_read_scope_list dans
libfreerdp/core/license.c dans FreeRDP permettait à des serveurs RDP distants
de provoquer un déni de service (plantage d'application) ou, éventuellement,
d’avoir un impact non précisé à l'aide d'une grande valeur ScopeCount dans une
Scope List dans un paquet Server License Request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11042">CVE-2020-11042</a>

<p>Dans FreeRDP, il existait une lecture hors limites dans update_read_icon_info.
Elle permettait de lire un montant défini par l’attaquant de mémoire client
(32 bits non signé -> 4GB) d’un tampon intermédiaire. Cela pourrait avoir été
utilisé pour planter le client ou stocker de l’information pour une récupération
ultérieure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11045">CVE-2020-11045</a>

<p>Dans FreeRDP, il existait une lecture hors limites dans
update_read_bitmap_data qui permettait une lecture de la mémoire client d’un
tampon image. Le résultat était affiché en couleur sur l’écran.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11046">CVE-2020-11046</a>

<p>Dans FreeRDP, il existait une recherche de flux hors limites dans
update_read_synchronize qui pouvait conduire à une lecture ultérieure hors
limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11048">CVE-2020-11048</a>

<p>Dans FreeRDP, il existait une lecture hors limites. Elle permettait
d’interrompre une session. Aucune extraction de données n’était possible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11058">CVE-2020-11058</a>

<p>Dans FreeRDP, une recherche de flux hors limites dans
rdp_read_font_capability_set pouvait conduire à une lecture ultérieure hors
limites. En conséquence, un serveur ou un client manipulé pouvait être obligé de
se déconnecter à cause d’une lecture non valable de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11521">CVE-2020-11521</a>

<p>libfreerdp/codec/planar.c dans FreeRDP avait une écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11522">CVE-2020-11522</a>

<p>libfreerdp/gdi/gdi.c dans FreeRDP avait une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11523">CVE-2020-11523</a>

<p>libfreerdp/gdi/region.c dans FreeRDP avait un dépassement d'entier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11525">CVE-2020-11525</a>

<p>libfreerdp/cache/bitmap.c dans FreeRDP avait une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11526">CVE-2020-11526</a>

<p>libfreerdp/core/update.c dans FreeRDP avait une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13396">CVE-2020-13396</a>

<p>Une vulnérabilité de lecture hors limites (OOB) a été détectée dans
ntlm_read_ChallengeMessage dans winpr/libwinpr/sspi/NTLM/ntlm_message.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13397">CVE-2020-13397</a>

<p>Une vulnérabilité de lecture hors limites (OOB) a été détectée dans
security_fips_decrypt dans libfreerdp/core/security.c due à une valeur non
initialisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13398">CVE-2020-13398</a>

<p>Une vulnérabilité de lecture hors limites (OOB) a été détectée dans
crypto_rsa_common dans libfreerdp/crypto/crypto.c.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.1.0~git20140921.1.440916e+dfsg1-13+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freerdp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de freerdp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/freerdp">https://security-tracker.debian.org/tracker/freerdp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2356.data"
# $Id: $
