#use wml::debian::translation-check translation="12c4cca978047502571c8088389091e82d309993" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans MediaWiki, un moteur
de site web pour travail collaboratif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20270">CVE-2021-20270</a>

<p>Une boucle infinie dans SMLLexer dans Pygments, utilisé par mediawiki comme
un de ses analyseurs lexicaux, pourrait conduire à un déni de service lors de
la coloration syntaxique d’un fichier source Standard ML (SML), comme le montre
une entrée contenant uniquement le mot-clé <q>exception</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27291">CVE-2021-27291</a>

<p>Pygments et les analyseurs syntaxiques utilisés par mediawiki reposent
fortement sur les expressions rationnelles. Certaines de ces expressions
rationnelles ont une complexité dans le pire des cas exponentielles ou cubiques
et sont vulnérables à un déni de service par expression rationnelle. En
trafiquant une entrée, un attaquant peut provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30152">CVE-2021-30152</a>

<p>Un problème a été découvert dans MediaWiki. Lors de l’utilisation de l’API
MediaWiki pour <q>protéger</q> une page, un utilisateur pourrait être
capable de protéger à un niveau plus haut que ses permissions lui permettent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30155">CVE-2021-30155</a>

<p>Un problème a été découvert dans MediaWiki. ContentModelChange ne vérifie pas
si un utilisateur à les permissions correctes pour créer et définir le modèle
de contenu pour une page non existante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30158">CVE-2021-30158</a>

<p>Un problème a été découvert dans MediaWiki. Les utilisateurs bloqués sont
incapables d’utiliser Special:ResetTokens. Cela relève de la sécurité, car un
utilisateur bloqué pourrait accidentellement avoir partagé un jeton ou pourrait
savoir qu’un jeton a été compromis et de plus serait incapable de bloquer une
utilisation potentielle du jeton par un tiers non autorisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30159">CVE-2021-30159</a>

<p>Un problème a été découvert dans MediaWiki. Les utilisateurs pourraient
contourner les restrictions voulues de suppression de pages dans certaines
situations <q>fast double move</q>. MovePage::isValidMoveTarget() utilise FOR
UPDATE, mais il est seulement appelé si Title::getArticleID() renvoie une
valeur différente de zéro sans indicateurs spéciaux. De plus,
MovePage::moveToInternal() supprimera la page si getArticleID(READ_LATEST) est
différent de zéro. Par conséquent, si la page est manquante dans la copie de
base de données, isValidMove() renverra vrai et alors moveToInternal()
supprimera inconditionnellement la page si elle peut être trouvée dans le
document maître.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.27.7-1~deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mediawiki">\
https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2648.data"
# $Id: $
