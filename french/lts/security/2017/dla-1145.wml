#use wml::debian::translation-check translation="7e6b86e512d1e203a20619cdee7230be27c2eae5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans zoneminder. Cette mise
à jour corrige une vulnérabilité sérieuse de divulgation de fichier
(<a href="https://security-tracker.debian.org/tracker/CVE-2017-5595">CVE-2017-5595</a>).</p>

<p>L’application s’est avérée comme étant sujette à de nombreux problèmes tels
que des vulnérabilités d’injection SQL, des problèmes de script intersite, des
contrefaçons de requête intersite et une vulnérabilité de fixation de session.
À cause du nombre de problèmes et l’invasivité relative des correctifs
concernés, ces problèmes ne seront pas corrigés dans Wheezy. Par conséquent,
nous vous conseillons de restreindre l’accès à zoneminder aux utilisateurs
fiables. Si vous voulez réexaminer la listes des problèmes ignorés, vous pouvez
consulter le suivi de sécurité :
<a href="https://security-tracker.debian.org/tracker/source-package/zoneminder">https://security-tracker.debian.org/tracker/source-package/zoneminder</a></p>

<p>Nous vous recommandons de mettre à jour vos paquets zoneminder.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p> Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été résolus dans la
version 1.25.0-4+deb7u2</p> de zoneminder
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1145.data"
# $Id: $
